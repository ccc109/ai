# 中小學電腦補充教材

* 十二年國教課程綱
    * https://www.naer.edu.tw/files/15-1000-14113,c639-1.php?Lang=zh-tw
    * 數學 -- https://www.naer.edu.tw/ezfiles/0/1000/attach/49/pta_18524_6629744_60029.pdf
    * 科技 -- https://www.naer.edu.tw/ezfiles/0/1000/attach/52/pta_18529_8438379_60115.pdf
    * 自然科學 -- https://www.naer.edu.tw/ezfiles/0/1000/attach/63/pta_18538_240851_60502.pdf
    