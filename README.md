# 人工智慧課程

欄位          | 說明
--------------|------------------------
學校科系       | [金門大學](https://www.nqu.edu.tw/) / [資訊工程系](https://www.nqu.edu.tw/educsie/)
開課教師       | [陳鍾誠](../../)
程式語言       | Python
難度           | 從最簡單開始，逐漸深入
教材           | [課程地圖](map.md)
程式           | https://github.com/cccschool/ai

