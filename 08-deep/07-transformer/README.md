# Transformer

* https://github.com/huggingface/transformers#installation


Important To run the latest versions of the examples, you have to install from source and install some specific requirements for the examples. Execute the following steps in a new virtual environment:

```
git clone https://github.com/huggingface/transformers
cd transformers
pip install .
pip install -r ./examples/requirements.txt
```


## 莫凡 NLP

* https://github.com/MorvanZhou/NLP-Tutorials
    * https://mofanpy.com/tutorials/machine-learning/nlp/
    * [Transformer 自注意语言模型 #5.4 (莫烦Python NLP 自然语言处理教学)](https://www.youtube.com/watch?v=KBl3ZTeLqdo)
