# cifar10 -- 10 種影像物件辨識

## 參考

* https://ithelp.ithome.com.tw/articles/10218698
* https://pytorch.org/tutorials/beginner/blitz/cifar10_tutorial.html

## predict

```
mac020:cifar mac020$ python3 cifar.py predict
device= cpu
Files already downloaded and verified
Files already downloaded and verified
Answer:  bird  frog   cat truck
Predicted:   bird  frog   cat truck
```

## test

```
mac020:cifar mac020$ python3 cifar.py test
device= cpu
Files already downloaded and verified
Files already downloaded and verified
Accuracy of the network on the 10000 test images: 58 %
```

## train

```
mac020:02-cifar mac020$ python3 cifar.py train
device= cpu
Files already downloaded and verified
Files already downloaded and verified
[1,  2000] loss: 2.195
[1,  4000] loss: 1.915
[1,  6000] loss: 1.689
[1,  8000] loss: 1.604
[1, 10000] loss: 1.522
[1, 12000] loss: 1.473
[2,  2000] loss: 1.411
[2,  4000] loss: 1.374
[2,  6000] loss: 1.350
[2,  8000] loss: 1.361
[2, 10000] loss: 1.329
[2, 12000] loss: 1.313
[3,  2000] loss: 1.244
[3,  4000] loss: 1.249
[3,  6000] loss: 1.220
[3,  8000] loss: 1.240
[3, 10000] loss: 1.209
[3, 12000] loss: 1.209
Finished Training
mac020:02-cifar mac020$ python3 cifar.py test
device= cpu
Files already downloaded and verified
Files already downloaded and verified
Accuracy of the network on the 10000 test images: 57 %
mac020:02-cifar mac020$ python3 cifar.py predict
device= cpu
Files already downloaded and verified
Files already downloaded and verified
2021-06-03 10:00:57.025 Python[7438:152724] ApplePersistenceIgnoreState: Existing state will not be touched. New state will be written to /var/folders/fc/784k2dvx3gvfq5h2p27vxb8m0000gn/T/org.python.python.savedState
Answer: plane plane   car  frog
Predicted:  truck  bird truck  frog
mac020:02-cifar mac020$ python3 cifar.py predict
device= cpu
Files already downloaded and verified
Files already downloaded and verified
2021-06-03 10:01:49.167 Python[7495:154123] ApplePersistenceIgnoreState: Existing state will not be touched. New state will be written to /var/folders/fc/784k2dvx3gvfq5h2p27vxb8m0000gn/T/org.python.python.savedState
Answer: truck  frog  bird horse
Predicted:  truck  deer  bird horse
```

