# MNIST 手寫辨識



* [MNIST Handwritten Digit Recognition in PyTorch](https://nextjournal.com/gkoehler/pytorch-mnist)

```
mac020:01-mnist mac020$ python3 mnist2.py
Downloading http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
Failed to download (trying next):
HTTP Error 503: Service Unavailable

Downloading https://ossci-datasets.s3.amazonaws.com/mnist/train-images-idx3-ubyte.gz
Downloading https://ossci-datasets.s3.amazonaws.com/mnist/train-images-idx3-ubyte.gz to files/MNIST/raw/train-images-idx3-ubyte.gz
9913344it [00:09, 1007229.58it/s]                                                
Extracting files/MNIST/raw/train-images-idx3-ubyte.gz to files/MNIST/raw

Downloading http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
Downloading http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz to files/MNIST/raw/train-labels-idx1-ubyte.gz
29696it [00:00, 385867.13it/s]                                                   
Extracting files/MNIST/raw/train-labels-idx1-ubyte.gz to files/MNIST/raw

Downloading http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
Downloading http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz to files/MNIST/raw/t10k-images-idx3-ubyte.gz
Failed to download (trying next):
HTTP Error 503: Service Unavailable

Downloading https://ossci-datasets.s3.amazonaws.com/mnist/t10k-images-idx3-ubyte.gz
Downloading https://ossci-datasets.s3.amazonaws.com/mnist/t10k-images-idx3-ubyte.gz to files/MNIST/raw/t10k-images-idx3-ubyte.gz
1649664it [00:02, 709915.23it/s]                                                 
Extracting files/MNIST/raw/t10k-images-idx3-ubyte.gz to files/MNIST/raw

Downloading http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
Downloading http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz to files/MNIST/raw/t10k-labels-idx1-ubyte.gz
5120it [00:00, 2046977.07it/s]                                                   
Extracting files/MNIST/raw/t10k-labels-idx1-ubyte.gz to files/MNIST/raw

Processing...
/usr/local/lib/python3.9/site-packages/torchvision/datasets/mnist.py:502: UserWarning: The given NumPy array is not writeable, and PyTorch does not support non-writeable tensors. This means you can write to the underlying (supposedly non-writeable) NumPy array using the tensor. You may want to copy the array to protect its data or make it writeable before converting it to a tensor. This type of warning will be suppressed for the rest of this program. (Triggered internally at  ../torch/csrc/utils/tensor_numpy.cpp:143.)
  return torch.from_numpy(parsed.astype(m[2], copy=False)).view(*s)
Done!
2021-06-03 09:05:38.247 Python[3525:69685] ApplePersistenceIgnoreState: Existing state will not be touched. New state will be written to /var/folders/fc/784k2dvx3gvfq5h2p27vxb8m0000gn/T/org.python.python.savedState
/Users/mac020/Desktop/ccc/ai/08-deep/03-cnn/01-mnist/mnist2.py:71: UserWarning: Implicit dimension choice for log_softmax has been deprecated. Change the call to include dim=X as an argument.
  return F.log_softmax(x)
/usr/local/lib/python3.9/site-packages/torch/nn/_reduction.py:42: UserWarning: size_average and reduce args will be deprecated, please use reduction='sum' instead.
  warnings.warn(warning.format(ret))

Test set: Avg. loss: 2.3089, Accuracy: 674/10000 (7%)

Train Epoch: 1 [0/60000 (0%)]   Loss: 2.368649
Traceback (most recent call last):
  File "/Users/mac020/Desktop/ccc/ai/08-deep/03-cnn/01-mnist/mnist2.py", line 119, in <module>
    train(epoch)
  File "/Users/mac020/Desktop/ccc/ai/08-deep/03-cnn/01-mnist/mnist2.py", line 98, in train
    torch.save(network.state_dict(), 'results/model.pth')
  File "/usr/local/lib/python3.9/site-packages/torch/serialization.py", line 369, in save
    with _open_file_like(f, 'wb') as opened_file:
  File "/usr/local/lib/python3.9/site-packages/torch/serialization.py", line 230, in _open_file_like
    return _open_file(name_or_buffer, mode)
  File "/usr/local/lib/python3.9/site-packages/torch/serialization.py", line 211, in __init__
    super(_open_file, self).__init__(open(name, mode))
FileNotFoundError: [Errno 2] No such file or directory: 'results/model.pth'
mac020:01-mnist mac020$ python3 mnist2.py
2021-06-03 09:06:04.955 Python[3654:71324] ApplePersistenceIgnoreState: Existing state will not be touched. New state will be written to /var/folders/fc/784k2dvx3gvfq5h2p27vxb8m0000gn/T/org.python.python.savedState
/Users/mac020/Desktop/ccc/ai/08-deep/03-cnn/01-mnist/mnist2.py:71: UserWarning: Implicit dimension choice for log_softmax has been deprecated. Change the call to include dim=X as an argument.
  return F.log_softmax(x)
/usr/local/lib/python3.9/site-packages/torch/nn/_reduction.py:42: UserWarning: size_average and reduce args will be deprecated, please use reduction='sum' instead.
  warnings.warn(warning.format(ret))

Test set: Avg. loss: 2.3089, Accuracy: 674/10000 (7%)

Train Epoch: 1 [0/60000 (0%)]   Loss: 2.368649
Train Epoch: 1 [640/60000 (1%)] Loss: 2.298366
Train Epoch: 1 [1280/60000 (2%)]        Loss: 2.294433
Train Epoch: 1 [1920/60000 (3%)]        Loss: 2.260970
Train Epoch: 1 [2560/60000 (4%)]        Loss: 2.295451
Train Epoch: 1 [3200/60000 (5%)]        Loss: 2.239505
Train Epoch: 1 [3840/60000 (6%)]        Loss: 2.274136
Train Epoch: 1 [4480/60000 (7%)]        Loss: 2.225712
Train Epoch: 1 [5120/60000 (9%)]        Loss: 2.168034
Train Epoch: 1 [5760/60000 (10%)]       Loss: 2.138136
Train Epoch: 1 [6400/60000 (11%)]       Loss: 2.061490
Train Epoch: 1 [7040/60000 (12%)]       Loss: 1.944445
Train Epoch: 1 [7680/60000 (13%)]       Loss: 1.947590
Train Epoch: 1 [8320/60000 (14%)]       Loss: 1.750810
Train Epoch: 1 [8960/60000 (15%)]       Loss: 1.642462
Train Epoch: 1 [9600/60000 (16%)]       Loss: 1.586518
Train Epoch: 1 [10240/60000 (17%)]      Loss: 1.416933
Train Epoch: 1 [10880/60000 (18%)]      Loss: 1.320050
Train Epoch: 1 [11520/60000 (19%)]      Loss: 1.254978
Train Epoch: 1 [12160/60000 (20%)]      Loss: 1.342793
Train Epoch: 1 [12800/60000 (21%)]      Loss: 1.076161
Train Epoch: 1 [13440/60000 (22%)]      Loss: 1.088678
Train Epoch: 1 [14080/60000 (23%)]      Loss: 0.989326
Train Epoch: 1 [14720/60000 (25%)]      Loss: 1.040402
Train Epoch: 1 [15360/60000 (26%)]      Loss: 0.895088
Train Epoch: 1 [16000/60000 (27%)]      Loss: 0.920773
Train Epoch: 1 [16640/60000 (28%)]      Loss: 0.942354
Train Epoch: 1 [17280/60000 (29%)]      Loss: 0.758985
Train Epoch: 1 [17920/60000 (30%)]      Loss: 0.905197
Train Epoch: 1 [18560/60000 (31%)]      Loss: 0.859492
Train Epoch: 1 [19200/60000 (32%)]      Loss: 0.829317
Train Epoch: 1 [19840/60000 (33%)]      Loss: 0.802275
Train Epoch: 1 [20480/60000 (34%)]      Loss: 0.998475
Train Epoch: 1 [21120/60000 (35%)]      Loss: 0.748636
Train Epoch: 1 [21760/60000 (36%)]      Loss: 0.777955
Train Epoch: 1 [22400/60000 (37%)]      Loss: 0.765506
Train Epoch: 1 [23040/60000 (38%)]      Loss: 0.991170
Train Epoch: 1 [23680/60000 (39%)]      Loss: 0.617360
Train Epoch: 1 [24320/60000 (41%)]      Loss: 0.803385
Train Epoch: 1 [24960/60000 (42%)]      Loss: 0.645416
Train Epoch: 1 [25600/60000 (43%)]      Loss: 0.582731
Train Epoch: 1 [26240/60000 (44%)]      Loss: 0.710625
Train Epoch: 1 [26880/60000 (45%)]      Loss: 0.536113
Train Epoch: 1 [27520/60000 (46%)]      Loss: 0.581613
Train Epoch: 1 [28160/60000 (47%)]      Loss: 0.626446
Train Epoch: 1 [28800/60000 (48%)]      Loss: 0.683360
Train Epoch: 1 [29440/60000 (49%)]      Loss: 0.519485
Train Epoch: 1 [30080/60000 (50%)]      Loss: 0.655357
Train Epoch: 1 [30720/60000 (51%)]      Loss: 0.633715
Train Epoch: 1 [31360/60000 (52%)]      Loss: 0.711431
Train Epoch: 1 [32000/60000 (53%)]      Loss: 0.495706
Train Epoch: 1 [32640/60000 (54%)]      Loss: 0.670235
Train Epoch: 1 [33280/60000 (55%)]      Loss: 0.503611
Train Epoch: 1 [33920/60000 (57%)]      Loss: 0.661733
Train Epoch: 1 [34560/60000 (58%)]      Loss: 0.518765
Train Epoch: 1 [35200/60000 (59%)]      Loss: 0.463650
Train Epoch: 1 [35840/60000 (60%)]      Loss: 0.776236
Train Epoch: 1 [36480/60000 (61%)]      Loss: 0.576281
Train Epoch: 1 [37120/60000 (62%)]      Loss: 0.734464
Train Epoch: 1 [37760/60000 (63%)]      Loss: 0.785822
Train Epoch: 1 [38400/60000 (64%)]      Loss: 0.368684
Train Epoch: 1 [39040/60000 (65%)]      Loss: 0.695281
Train Epoch: 1 [39680/60000 (66%)]      Loss: 0.462430
Train Epoch: 1 [40320/60000 (67%)]      Loss: 0.467661
Train Epoch: 1 [40960/60000 (68%)]      Loss: 0.597138
Train Epoch: 1 [41600/60000 (69%)]      Loss: 0.741421
Train Epoch: 1 [42240/60000 (70%)]      Loss: 0.657079
Train Epoch: 1 [42880/60000 (71%)]      Loss: 0.570380
Train Epoch: 1 [43520/60000 (72%)]      Loss: 0.613471
Train Epoch: 1 [44160/60000 (74%)]      Loss: 0.369303
Train Epoch: 1 [44800/60000 (75%)]      Loss: 0.600221
Train Epoch: 1 [45440/60000 (76%)]      Loss: 0.340070
Train Epoch: 1 [46080/60000 (77%)]      Loss: 0.578646
Train Epoch: 1 [46720/60000 (78%)]      Loss: 0.380396
Train Epoch: 1 [47360/60000 (79%)]      Loss: 0.503065
Train Epoch: 1 [48000/60000 (80%)]      Loss: 0.749657
Train Epoch: 1 [48640/60000 (81%)]      Loss: 0.709014
Train Epoch: 1 [49280/60000 (82%)]      Loss: 0.678653
Train Epoch: 1 [49920/60000 (83%)]      Loss: 0.721201
Train Epoch: 1 [50560/60000 (84%)]      Loss: 0.901344
Train Epoch: 1 [51200/60000 (85%)]      Loss: 0.531224
Train Epoch: 1 [51840/60000 (86%)]      Loss: 0.555073
Train Epoch: 1 [52480/60000 (87%)]      Loss: 0.436439
Train Epoch: 1 [53120/60000 (88%)]      Loss: 0.350076
Train Epoch: 1 [53760/60000 (90%)]      Loss: 0.493331
Train Epoch: 1 [54400/60000 (91%)]      Loss: 0.527324
Train Epoch: 1 [55040/60000 (92%)]      Loss: 0.537204
Train Epoch: 1 [55680/60000 (93%)]      Loss: 0.534056
Train Epoch: 1 [56320/60000 (94%)]      Loss: 0.358716
Train Epoch: 1 [56960/60000 (95%)]      Loss: 0.555209
Train Epoch: 1 [57600/60000 (96%)]      Loss: 0.561935
Train Epoch: 1 [58240/60000 (97%)]      Loss: 0.475567
Train Epoch: 1 [58880/60000 (98%)]      Loss: 0.339157
Train Epoch: 1 [59520/60000 (99%)]      Loss: 0.435705

Test set: Avg. loss: 0.1936, Accuracy: 9441/10000 (94%)

Train Epoch: 2 [0/60000 (0%)]   Loss: 0.384471
Train Epoch: 2 [640/60000 (1%)] Loss: 0.507132
Train Epoch: 2 [1280/60000 (2%)]        Loss: 0.481464
Train Epoch: 2 [1920/60000 (3%)]        Loss: 0.398343
Train Epoch: 2 [2560/60000 (4%)]        Loss: 0.561240
Train Epoch: 2 [3200/60000 (5%)]        Loss: 0.594501
Train Epoch: 2 [3840/60000 (6%)]        Loss: 0.288780
Train Epoch: 2 [4480/60000 (7%)]        Loss: 0.709731
Train Epoch: 2 [5120/60000 (9%)]        Loss: 0.460899
Train Epoch: 2 [5760/60000 (10%)]       Loss: 0.472801
Train Epoch: 2 [6400/60000 (11%)]       Loss: 0.419338
Train Epoch: 2 [7040/60000 (12%)]       Loss: 0.662510
Train Epoch: 2 [7680/60000 (13%)]       Loss: 0.510178
Train Epoch: 2 [8320/60000 (14%)]       Loss: 0.496903
Train Epoch: 2 [8960/60000 (15%)]       Loss: 0.490112
Train Epoch: 2 [9600/60000 (16%)]       Loss: 0.389237
Train Epoch: 2 [10240/60000 (17%)]      Loss: 0.429269
Train Epoch: 2 [10880/60000 (18%)]      Loss: 0.313150
Train Epoch: 2 [11520/60000 (19%)]      Loss: 0.374846
Train Epoch: 2 [12160/60000 (20%)]      Loss: 0.341214
Train Epoch: 2 [12800/60000 (21%)]      Loss: 0.529734
Train Epoch: 2 [13440/60000 (22%)]      Loss: 0.411694
Train Epoch: 2 [14080/60000 (23%)]      Loss: 0.370910
Train Epoch: 2 [14720/60000 (25%)]      Loss: 0.320859
Train Epoch: 2 [15360/60000 (26%)]      Loss: 0.402550
Train Epoch: 2 [16000/60000 (27%)]      Loss: 0.369091
Train Epoch: 2 [16640/60000 (28%)]      Loss: 0.442823
Train Epoch: 2 [17280/60000 (29%)]      Loss: 0.381754
Train Epoch: 2 [17920/60000 (30%)]      Loss: 0.651142
Train Epoch: 2 [18560/60000 (31%)]      Loss: 0.391318
Train Epoch: 2 [19200/60000 (32%)]      Loss: 0.301943
Train Epoch: 2 [19840/60000 (33%)]      Loss: 0.266307
Train Epoch: 2 [20480/60000 (34%)]      Loss: 0.409143
Train Epoch: 2 [21120/60000 (35%)]      Loss: 0.272691
Train Epoch: 2 [21760/60000 (36%)]      Loss: 0.274691
Train Epoch: 2 [22400/60000 (37%)]      Loss: 0.421387
Train Epoch: 2 [23040/60000 (38%)]      Loss: 0.537888
Train Epoch: 2 [23680/60000 (39%)]      Loss: 0.431688
Train Epoch: 2 [24320/60000 (41%)]      Loss: 0.282804
Train Epoch: 2 [24960/60000 (42%)]      Loss: 0.409435
Train Epoch: 2 [25600/60000 (43%)]      Loss: 0.562904
Train Epoch: 2 [26240/60000 (44%)]      Loss: 0.288971
Train Epoch: 2 [26880/60000 (45%)]      Loss: 0.569597
Train Epoch: 2 [27520/60000 (46%)]      Loss: 0.520910
Train Epoch: 2 [28160/60000 (47%)]      Loss: 0.426153
Train Epoch: 2 [28800/60000 (48%)]      Loss: 0.414259
Train Epoch: 2 [29440/60000 (49%)]      Loss: 0.373608
Train Epoch: 2 [30080/60000 (50%)]      Loss: 0.562746
Train Epoch: 2 [30720/60000 (51%)]      Loss: 0.278834
Train Epoch: 2 [31360/60000 (52%)]      Loss: 0.387222
Train Epoch: 2 [32000/60000 (53%)]      Loss: 0.246794
Train Epoch: 2 [32640/60000 (54%)]      Loss: 0.295881
Train Epoch: 2 [33280/60000 (55%)]      Loss: 0.330927
Train Epoch: 2 [33920/60000 (57%)]      Loss: 0.301844
Train Epoch: 2 [34560/60000 (58%)]      Loss: 0.251977
Train Epoch: 2 [35200/60000 (59%)]      Loss: 0.684123
Train Epoch: 2 [35840/60000 (60%)]      Loss: 0.289180
Train Epoch: 2 [36480/60000 (61%)]      Loss: 0.360711
Train Epoch: 2 [37120/60000 (62%)]      Loss: 0.384347
Train Epoch: 2 [37760/60000 (63%)]      Loss: 0.345516
Train Epoch: 2 [38400/60000 (64%)]      Loss: 0.333049
Train Epoch: 2 [39040/60000 (65%)]      Loss: 0.420416
Train Epoch: 2 [39680/60000 (66%)]      Loss: 0.267250
Train Epoch: 2 [40320/60000 (67%)]      Loss: 0.448465
Train Epoch: 2 [40960/60000 (68%)]      Loss: 0.225693
Train Epoch: 2 [41600/60000 (69%)]      Loss: 0.203764
Train Epoch: 2 [42240/60000 (70%)]      Loss: 0.206746
Train Epoch: 2 [42880/60000 (71%)]      Loss: 0.420243
Train Epoch: 2 [43520/60000 (72%)]      Loss: 0.507957
Train Epoch: 2 [44160/60000 (74%)]      Loss: 0.532539
Train Epoch: 2 [44800/60000 (75%)]      Loss: 0.262245
Train Epoch: 2 [45440/60000 (76%)]      Loss: 0.405699
Train Epoch: 2 [46080/60000 (77%)]      Loss: 0.339553
Train Epoch: 2 [46720/60000 (78%)]      Loss: 0.394878
Train Epoch: 2 [47360/60000 (79%)]      Loss: 0.317512
Train Epoch: 2 [48000/60000 (80%)]      Loss: 0.294394
Train Epoch: 2 [48640/60000 (81%)]      Loss: 0.296049
Train Epoch: 2 [49280/60000 (82%)]      Loss: 0.333770
Train Epoch: 2 [49920/60000 (83%)]      Loss: 0.164180
Train Epoch: 2 [50560/60000 (84%)]      Loss: 0.334922
Train Epoch: 2 [51200/60000 (85%)]      Loss: 0.288940
Train Epoch: 2 [51840/60000 (86%)]      Loss: 0.326070
Train Epoch: 2 [52480/60000 (87%)]      Loss: 0.220732
Train Epoch: 2 [53120/60000 (88%)]      Loss: 0.604859
Train Epoch: 2 [53760/60000 (90%)]      Loss: 0.493199
Train Epoch: 2 [54400/60000 (91%)]      Loss: 0.362413
Train Epoch: 2 [55040/60000 (92%)]      Loss: 0.418334
Train Epoch: 2 [55680/60000 (93%)]      Loss: 0.461943
Train Epoch: 2 [56320/60000 (94%)]      Loss: 0.364452
Train Epoch: 2 [56960/60000 (95%)]      Loss: 0.167465
Train Epoch: 2 [57600/60000 (96%)]      Loss: 0.490835
Train Epoch: 2 [58240/60000 (97%)]      Loss: 0.350615
Train Epoch: 2 [58880/60000 (98%)]      Loss: 0.271122
Train Epoch: 2 [59520/60000 (99%)]      Loss: 0.280188

Test set: Avg. loss: 0.1197, Accuracy: 9647/10000 (96%)

Train Epoch: 3 [0/60000 (0%)]   Loss: 0.340067
Train Epoch: 3 [640/60000 (1%)] Loss: 0.268787
Train Epoch: 3 [1280/60000 (2%)]        Loss: 0.327552
Train Epoch: 3 [1920/60000 (3%)]        Loss: 0.400870
Train Epoch: 3 [2560/60000 (4%)]        Loss: 0.384394
Train Epoch: 3 [3200/60000 (5%)]        Loss: 0.200910
Train Epoch: 3 [3840/60000 (6%)]        Loss: 0.359894
Train Epoch: 3 [4480/60000 (7%)]        Loss: 0.114099
Train Epoch: 3 [5120/60000 (9%)]        Loss: 0.200374
Train Epoch: 3 [5760/60000 (10%)]       Loss: 0.434872
Train Epoch: 3 [6400/60000 (11%)]       Loss: 0.253498
Train Epoch: 3 [7040/60000 (12%)]       Loss: 0.330702
Train Epoch: 3 [7680/60000 (13%)]       Loss: 0.236153
Train Epoch: 3 [8320/60000 (14%)]       Loss: 0.251310
Train Epoch: 3 [8960/60000 (15%)]       Loss: 0.220727
Train Epoch: 3 [9600/60000 (16%)]       Loss: 0.342837
Train Epoch: 3 [10240/60000 (17%)]      Loss: 0.157662
Train Epoch: 3 [10880/60000 (18%)]      Loss: 0.270703
Train Epoch: 3 [11520/60000 (19%)]      Loss: 0.297923
Train Epoch: 3 [12160/60000 (20%)]      Loss: 0.230377
Train Epoch: 3 [12800/60000 (21%)]      Loss: 0.373893
Train Epoch: 3 [13440/60000 (22%)]      Loss: 0.496462
Train Epoch: 3 [14080/60000 (23%)]      Loss: 0.245390
Train Epoch: 3 [14720/60000 (25%)]      Loss: 0.278857
Train Epoch: 3 [15360/60000 (26%)]      Loss: 0.449371
Train Epoch: 3 [16000/60000 (27%)]      Loss: 0.288121
Train Epoch: 3 [16640/60000 (28%)]      Loss: 0.286473
Train Epoch: 3 [17280/60000 (29%)]      Loss: 0.162235
Train Epoch: 3 [17920/60000 (30%)]      Loss: 0.352848
Train Epoch: 3 [18560/60000 (31%)]      Loss: 0.162209
Train Epoch: 3 [19200/60000 (32%)]      Loss: 0.232859
Train Epoch: 3 [19840/60000 (33%)]      Loss: 0.253972
Train Epoch: 3 [20480/60000 (34%)]      Loss: 0.336535
Train Epoch: 3 [21120/60000 (35%)]      Loss: 0.171988
Train Epoch: 3 [21760/60000 (36%)]      Loss: 0.484350
Train Epoch: 3 [22400/60000 (37%)]      Loss: 0.322516
Train Epoch: 3 [23040/60000 (38%)]      Loss: 0.381638
Train Epoch: 3 [23680/60000 (39%)]      Loss: 0.230062
Train Epoch: 3 [24320/60000 (41%)]      Loss: 0.391318
Train Epoch: 3 [24960/60000 (42%)]      Loss: 0.375078
Train Epoch: 3 [25600/60000 (43%)]      Loss: 0.334540
Train Epoch: 3 [26240/60000 (44%)]      Loss: 0.453939
Train Epoch: 3 [26880/60000 (45%)]      Loss: 0.177862
Train Epoch: 3 [27520/60000 (46%)]      Loss: 0.258441
Train Epoch: 3 [28160/60000 (47%)]      Loss: 0.357160
Train Epoch: 3 [28800/60000 (48%)]      Loss: 0.264182
Train Epoch: 3 [29440/60000 (49%)]      Loss: 0.237020
Train Epoch: 3 [30080/60000 (50%)]      Loss: 0.139710
Train Epoch: 3 [30720/60000 (51%)]      Loss: 0.178834
Train Epoch: 3 [31360/60000 (52%)]      Loss: 0.337134
Train Epoch: 3 [32000/60000 (53%)]      Loss: 0.255821
Train Epoch: 3 [32640/60000 (54%)]      Loss: 0.397162
Train Epoch: 3 [33280/60000 (55%)]      Loss: 0.277028
Train Epoch: 3 [33920/60000 (57%)]      Loss: 0.164946
Train Epoch: 3 [34560/60000 (58%)]      Loss: 0.221214
Train Epoch: 3 [35200/60000 (59%)]      Loss: 0.339639
Train Epoch: 3 [35840/60000 (60%)]      Loss: 0.284821
Train Epoch: 3 [36480/60000 (61%)]      Loss: 0.142788
Train Epoch: 3 [37120/60000 (62%)]      Loss: 0.139795
Train Epoch: 3 [37760/60000 (63%)]      Loss: 0.195139
Train Epoch: 3 [38400/60000 (64%)]      Loss: 0.238265
Train Epoch: 3 [39040/60000 (65%)]      Loss: 0.419794
Train Epoch: 3 [39680/60000 (66%)]      Loss: 0.251788
Train Epoch: 3 [40320/60000 (67%)]      Loss: 0.156784
Train Epoch: 3 [40960/60000 (68%)]      Loss: 0.245485
Train Epoch: 3 [41600/60000 (69%)]      Loss: 0.207012
Train Epoch: 3 [42240/60000 (70%)]      Loss: 0.199282
Train Epoch: 3 [42880/60000 (71%)]      Loss: 0.286683
Train Epoch: 3 [43520/60000 (72%)]      Loss: 0.229844
Train Epoch: 3 [44160/60000 (74%)]      Loss: 0.293832
Train Epoch: 3 [44800/60000 (75%)]      Loss: 0.301416
Train Epoch: 3 [45440/60000 (76%)]      Loss: 0.379130
Train Epoch: 3 [46080/60000 (77%)]      Loss: 0.602228
Train Epoch: 3 [46720/60000 (78%)]      Loss: 0.253018
Train Epoch: 3 [47360/60000 (79%)]      Loss: 0.220794
Train Epoch: 3 [48000/60000 (80%)]      Loss: 0.256666
Train Epoch: 3 [48640/60000 (81%)]      Loss: 0.279179
Train Epoch: 3 [49280/60000 (82%)]      Loss: 0.290726
Train Epoch: 3 [49920/60000 (83%)]      Loss: 0.328414
Train Epoch: 3 [50560/60000 (84%)]      Loss: 0.147755
Train Epoch: 3 [51200/60000 (85%)]      Loss: 0.258981
Train Epoch: 3 [51840/60000 (86%)]      Loss: 0.155041
Train Epoch: 3 [52480/60000 (87%)]      Loss: 0.431607
Train Epoch: 3 [53120/60000 (88%)]      Loss: 0.434785
Train Epoch: 3 [53760/60000 (90%)]      Loss: 0.249907
Train Epoch: 3 [54400/60000 (91%)]      Loss: 0.751697
Train Epoch: 3 [55040/60000 (92%)]      Loss: 0.366008
Train Epoch: 3 [55680/60000 (93%)]      Loss: 0.308781
Train Epoch: 3 [56320/60000 (94%)]      Loss: 0.242053
Train Epoch: 3 [56960/60000 (95%)]      Loss: 0.154638
Train Epoch: 3 [57600/60000 (96%)]      Loss: 0.335003
Train Epoch: 3 [58240/60000 (97%)]      Loss: 0.247836
Train Epoch: 3 [58880/60000 (98%)]      Loss: 0.185613
Train Epoch: 3 [59520/60000 (99%)]      Loss: 0.227668

Test set: Avg. loss: 0.0943, Accuracy: 9705/10000 (97%)
```