# 來源 -- https://www.geeksforgeeks.org/python-word-embedding-using-word2vec/
# Python program to generate word vectors using Word2Vec 
  
# importing all necessary modules
import nltk
# nltk.download('punkt')
from nltk.tokenize import sent_tokenize, word_tokenize 
import warnings 
  
warnings.filterwarnings(action = 'ignore') 
  
import gensim
from gensim.models import Word2Vec 
  
#  Reads ‘alice.txt’ file 
# sample = open("alice.txt", "r") 
sample = open("dogcat.txt", "r") 
s = sample.read() 
  
# Replaces escape character with space 
f = s.replace("\n", " . ") 
# f = s
  
data = [] 
  
# iterate through each sentence in the file 
for i in sent_tokenize(f): 
    temp = []
    # tokenize the sentence into words 
    for word in word_tokenize(i): 
        temp.append(word.lower()) 
  
    data.append(temp) 

print('data=', data)
# model = gensim.models.Word2Vec(data, min_count = 1, vector_size = 2, window = 1, sg=1, negative=3) 
model = gensim.models.Word2Vec(data, min_count = 1, vector_size = 2, window = 1, sg=1, negative=3) 

words = ['a', 'dog', 'cat', 'chase', 'eat', 'the']
for word in words:
	vector = model.wv[word]  # get numpy vector of a word
	print(f'{word} vector: {vector}')
	sims = model.wv.most_similar(word, topn=3)  # get other similar words
	print(f'    similars:{sims}')

'''
# Print results 
print("dog cat : ", model1.similarity('dog', 'cat')) 
print("cat dog : ", model1.similarity('cat', 'dog')) 
print("dog dog : ", model1.similarity('dog', 'dog')) 
print("dog eat : ", model1.similarity('dog', 'eat')) 
print("chase eat : ", model1.similarity('chase', 'eat'))
print("chase a : ", model1.similarity('chase', 'a')) 
print("dog a : ", model1.similarity('dog', 'a')) 
'''