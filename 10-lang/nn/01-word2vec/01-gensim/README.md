# gensim

* https://radimrehurek.com/gensim/models/word2vec.html (重要)
* https://radimrehurek.com/gensim/auto_examples/tutorials/run_word2vec.html
* https://machinelearningmastery.com/develop-word-embeddings-python-gensim/

## pretrained.py

```
PS D:\pmedia\陳鍾誠\課程\人工智慧\10-lang\nn\01-word2vec\01-gensim> python pretrained.py

model.most_similar('twitter')=[('facebook', 0.9480050802230835), ('tweet', 0.9403423070907593), ('fb', 0.9342359900474548), ('instagram', 0.9104824066162109), ('chat', 0.8964964747428894), ('hashtag', 0.8885937333106995), ('tweets', 0.8878158330917358), ('tl', 0.8778460621833801), ('link', 0.8778210878372192), ('internet', 0.8753897547721863)]
model.most_similar(positive=['woman', 'king'], negative=['man'])=[('meets', 0.8841924071311951), ('prince', 0.832163393497467), ('queen', 0.8257461190223694), ('’s', 0.8174097537994385), ('crow', 0.813499391078949), ('hunter', 0.8131037950515747), ('father', 0.8115834593772888), ('soldier', 0.81113600730896), ('mercy', 0.8082393407821655), ('hero', 0.8082264065742493)]
```

## trainCorpus.py

```
mac020:01-gensim mac020$ python3 trainCorpus.py
/usr/local/lib/python3.9/site-packages/gensim/similarities/__init__.py:15: UserWarning: The gensim.similarities.levenshtein submodule is disabled, because the optional Levenshtein package <https://pypi.org/project/python-Levenshtein/> is unavailable. Install Levenhstein (e.g. `pip install python-Levenshtein`) to suppress this warning.
  warnings.warn(msg)
model= Word2Vec(vocab=14, vector_size=100, alpha=0.025)
words= ['sentence', 'the', 'is', 'this', 'final', 'and', 'more', 'one', 'another', 'yet', 'second', 'word2vec', 'for', 'first']
vw(sentence)= [-5.3622725e-04  2.3643016e-04  5.1033497e-03  9.0092728e-03
 -9.3029495e-03 -7.1168090e-03  6.4588715e-03  8.9729885e-03
 -5.0154282e-03 -3.7633730e-03  7.3805046e-03 -1.5334726e-03
 -4.5366143e-03  6.5540504e-03 -4.8601604e-03 -1.8160177e-03
  2.8765798e-03  9.9187379e-04 -8.2852151e-03 -9.4488189e-03
  7.3117660e-03  5.0702621e-03  6.7576934e-03  7.6286553e-04
  6.3508893e-03 -3.4053659e-03 -9.4640255e-04  5.7685734e-03
 -7.5216386e-03 -3.9361049e-03 -7.5115822e-03 -9.3004224e-04
  9.5381187e-03 -7.3191668e-03 -2.3337698e-03 -1.9377422e-03
  8.0774352e-03 -5.9308959e-03  4.5161247e-05 -4.7537349e-03
 -9.6035507e-03  5.0072931e-03 -8.7595871e-03 -4.3918253e-03
 -3.5099984e-05 -2.9618264e-04 -7.6612402e-03  9.6147414e-03
  4.9820566e-03  9.2331432e-03 -8.1579182e-03  4.4957972e-03
 -4.1370774e-03  8.2453492e-04  8.4986184e-03 -4.4621779e-03
  4.5175003e-03 -6.7869616e-03 -3.5484887e-03  9.3985079e-03
 -1.5776539e-03  3.2137157e-04 -4.1406299e-03 -7.6826881e-03
 -1.5080094e-03  2.4697948e-03 -8.8802812e-04  5.5336617e-03
 -2.7429771e-03  2.2600652e-03  5.4557943e-03  8.3459523e-03
 -1.4537406e-03 -9.2081428e-03  4.3705511e-03  5.7178497e-04
  7.4419067e-03 -8.1328390e-04 -2.6384138e-03 -8.7530091e-03
 -8.5655687e-04  2.8265619e-03  5.4014279e-03  7.0526553e-03
 -5.7031228e-03  1.8588186e-03  6.0888622e-03 -4.7980524e-03
 -3.1072616e-03  6.7976285e-03  1.6314745e-03  1.8991709e-04
  3.4736372e-03  2.1777629e-04  9.6188262e-03  5.0606038e-03
 -8.9173913e-03 -7.0415614e-03  9.0145587e-04  6.3925339e-03]
new_model= Word2Vec(vocab=14, vector_size=100, alpha=0.025)
```

## viewTrained.py

```
mac020:01-gensim mac020$ python3 viewTrained.py
/usr/local/lib/python3.9/site-packages/gensim/similarities/__init__.py:15: UserWarning: The gensim.similarities.levenshtein submodule is disabled, because the optional Levenshtein package <https://pypi.org/project/python-Levenshtein/> is unavailable. Install Levenhstein (e.g. `pip install python-Levenshtein`) to suppress this warning.
  warnings.warn(msg)
2021-06-17 10:41:40.811 Python[27679:325690] ApplePersistenceIgnoreState: Existing state will not be touched. New state will be written to /var/folders/fc/784k2dvx3gvfq5h2p27vxb8m0000gn/T/org.python.python.savedState
```

![](./wordVectorView.png)

